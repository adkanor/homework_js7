"use strict";
// 1. Метод forЕach перебирает каждый елемент масива и позволяет применить функцию к каждому елементу масива.
// 2. Задать длинну масива - ноль
// 3.метод Array. isArray(), что бы проверить масив ли это или нет.

let anyArray = [0, "hello", 2345345, {}, false, "world", 23, "23"];
let dataType = 2345;

function filterBy(arr, value) {
  let newArray = [];
  let typeData = typeof value;
  for (let item of anyArray) {
    if (typeof item !== typeData) {
      newArray.push(item);
    }
  }
  return newArray;
}
console.log(filterBy(anyArray, dataType));
